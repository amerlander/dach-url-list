# HowTo: Gruppe in Liste eintragen

## Schritt 1: Neuen Branch anlegen

Klicke auf das Plus neben dach-url-list, und wähle "New branch"
![siehe /tut-img/Schritt-1.jpg](/tut-img/Schritt-1.jpg)
Im nächsten Fenster wählst du "Add-GROUPNAME" als Branchname und klickst "Create branch", du bist dann wieder auf der reposeite.

## Schritt 2: Gruppe zu Repo hinzufügen

Klicke auf Web IDE, diese öffnet sich daraufhin.
![siehe /tut-img/Schritt-2.jpg](/tut-img/Schritt-2.jpg)
Klicke dann auf die 3 Punkte neben groups und klicke auf "New File", bennene die datei wie deine gruppe und hänge ".json" am ende an.
Der Inhalt sieht dann in etwa folgendermaßen aus:

```
{"name": "der name deiner gruppe", "url": "https://t.me/Telegramlink-deiner-gruppe", "tags": ["Tags, die zu deiner gruppe passen"]}
```

Dann klicke unten links auf "Commit..." und schreibe eine Commitmessage a la "Adds Groupname to list".
Klicke dann auf "Commit".

## Schritt 3: Merge Request öffnen

Nun öffnet sich das Merge Request Fenster, du scrollst einfach nur nach unten und klickst "Create Merge Request".

Du bist von deiner seite aus fertig! Hurrah!
